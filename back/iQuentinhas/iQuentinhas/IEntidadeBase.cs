﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public interface IEntidadeBase<TId> where TId : IComparable<TId>, IEquatable<TId>
    {

        TId Id { get; set; }

    }
}
