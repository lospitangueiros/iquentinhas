﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class PontoVenda : EntidadeBase<long>
    {

        public string NomePontoVenda { get; set; }
        public string CEP { get; set; }
        public string Rua { get; set; }
        public string Numero { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        public IList<Avaliacao> Avaliacoes { get; set; }
        public Cardapio Cardapio { get; set; }
                   
    }
}
