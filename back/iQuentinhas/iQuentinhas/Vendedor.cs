﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
     public class Vendedor : Usuario
    {
        public string CPF { get; set; }
        public string CNPJ { get; set; }
        public PontoVenda PontoVenda { get; set; }
    }
}
